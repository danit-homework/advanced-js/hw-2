// Теоритичне завдання:
// 1. Робота з мережевими запитами, оскільки з'єднання з сервером може бути ненадійним
// або може бути проблема з підключенням до мережі.
// 2. Робота зі змінними, які можуть мати невірні значення, наприклад, 
// якщо користувач вводить некоректні дані.
// 3. Використання зовнішніх бібліотек, які можуть викликати помилки при неправильному використанні.
// 4. Використання складних алгоритмів, які можуть мати непередбачувані наслідки в разі помилки.

const books = [
	{
		author: "Скотт Бэккер",
		name: "Тьма, что приходит прежде",
		price: 70,
	},
	{
		author: "Скотт Бэккер",
		name: "Воин-пророк",
	},
	{
		name: "Тысячекратная мысль",
		price: 70,
	},
	{
		author: "Скотт Бэккер",
		name: "Нечестивый Консульт",
		price: 70,
	},
	{
		author: "Дарья Донцова",
		name: "Детектив на диете",
		price: 40,
	},
	{
		author: "Дарья Донцова",
		name: "Дед Снегур и Морозочка",
	},
];

const props = ["author", "name", "price"];

function validateBook(book) {
	for (let prop of props) {
		if (!book.hasOwnProperty(prop)) {
			throw new Error(`В книзі ${book.name} відсутня властивість ${prop}`);
		}
	}
	return book;
}

function createList(bookList) {
	const list = document.createElement("ul");
	for (let book of bookList) {
		const listBook = document.createElement("li");
		for (let key in book) {
			const p = document.createElement("p");
			p.textContent = `${key} : ${book[key]}`;
			listBook.append(p);
		}
		list.append(listBook);
	}
	return list;
}

function filterBooks(booksList, validationFunc) {
	const filteredBooks = booksList.filter((book) => {
		try {
			return validationFunc(book);
		}
		catch (error) {
			console.log(error);
			return false;
		}
	});
	return filteredBooks;
}

function renderBookList(bookList) {
	const root = document.createElement("div");
	root.id = "root";
	document.body.append(root);
	const validatedBooks = filterBooks(bookList, validateBook);
	const list = createList(validatedBooks);
	root.append(list);
}

renderBookList(books);